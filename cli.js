const _ = require("underscore");
const $$ = require("squeak/node");
require("squeak/extension/array");
let clj = require("clj");
let run = require("./src/run");
let commands = require("./src/commands");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: run gitty command, depending on cli arguments it was passed
  ARGUMENTS: ({
    !absolutePath: <"/path/to/root/of/git/repos/">,
    !modules: <{
      | [moduleName]: {
        !path: <"path/to/module/directory"> « path relative to previous "absolutePath" »,
        ?noBranches: <boolean> « if true, won't compare working and master branches and assume there's only a master branch »,
      },
      | [relative/path/to/moduleName]: <"master"|"working">,
    }>,
    ?detailedLogging: <boolean>@default=false «
      default value specifying commands verbosity level
      using the `-d` cli flag overwrites this to true
    »,
  })
  RETURN: <void>
*/
module.exports = async function (config) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DEFAULT CONFIG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var defaultConfig = {
    detailedLogging: false,
  };
  config = $$.defaults(defaultConfig, config);

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CLI SPECS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  const argv = await clj({
    name: "GITTY",
    description: "Check all apps and libs directories for necessity to merge from working branch, push changes...",
    noDefaultHelp: true,
    config: [
      {
        type: "positional",
        arg: "[moduleName] [commandName]",
        opts: {
          paramsDesc: [
            "If you want to act on one module only, specify it's name here.",
            "If you want to execute a command on this module, specify it here (see below for the list of valid commands).",
          ],
        },
      },
      {
        type: "boolean",
        arg: "-d, --details",
        opts: { desc: "Pass this to have additional logs." },
      },
      {
        type: "boolean",
        arg: "-g, --goodflags",
        opts: { desc: "Force show 'all good' messages when checking all repositories." },
      },
    ],
    additionalCommands: [
      {
        title: "Available commands:",
        content: _.map(commands, function (commandObject, commandName) {
          return [commandName, commandObject.desc];
        }),
      },
    ],
  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FIGURE OUT WHAT TO DO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              DETAILED LOGGING ASKED

  if (argv.details) config.detailedLogging = true;

  //
  //                              EXECUTE A SPECIFIC COMMAND OR DISPLAY STATS

  // figure out list of config categories
  var configCategories = _.chain(config.modules)
    .keys()
    .map(function (moduleName) {
      if (moduleName.match("/")) return moduleName.replace(/\/.*$/, "");
    })
    .uniq()
    .compact()
    .value()
  ;

  if (argv.commandName) run(config, argv.commandName, argv.moduleName)
  else {
    //
    //                              on a category of modules

    if (_.indexOf(configCategories, argv.moduleName) !== -1) {
      if (!argv.goodflags) config.hideAllGoodMessage = true;
      _.chain(config.modules).keys().reduce(async function (memo, moduleName) {
        await memo;
        if (moduleName.match(argv.moduleName)) await run(config, "checkall", moduleName);
      }, undefined)
    }

    //
    //                              on one module only

    else if (argv.moduleName) run(config, "checkall", argv.moduleName);

    //
    //                              on all modules

    else {
      if (!argv.goodflags) config.hideAllGoodMessage = true;
      await _.keys(config.modules).reduce(async function (memo, moduleName) {
        await memo; // this is useful to make sure iterations are happening serialy, and not in parallel (this is also why "reduce" is used and not forEach)
        await run(config, "checkall", moduleName);
      }, undefined);
    };

    //                              ¬
    //
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
