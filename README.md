# gitty

Light utility to check multiple git repositories status and run basic actions on them.

## Usage

- if you don't have it already, install [nodejs](https://nodejs.org/)
- clone this repository: `git clone https://framagit.org/squeak/gitty.git`
- cd in the repository just created
- install dependencies running `npm install`
- create a configuration file listing your git repositories to check (see `example-config.js` file for info on the syntax to use)
- make the `gitty` script executable with something like `chmod u+x ./gitty`
- run the `gitty` command, for example: `./gitty some-app`
