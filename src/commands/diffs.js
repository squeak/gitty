const _ = require("underscore");
const $$ = require("squeak/node");
require("squeak/plugin/compare");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  COMPARE PACKAGE.JSON FILE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// check if package.json is really modified, or just dependencies are
async function comparePackageJsonFile (modulo) {

  //
  //                              COMPARE PACKAGE.JSON FILES

  try {
    // git.silent(true); // make git silent otherwise it logs deprecation warning for using string as options for show, but without string cannot get desired result
    let masterFile = await modulo.git.show("master:package.json");
    let workingFile = await modulo.git.show("working:package.json");
    // git.silent(false); // remake git not silent, so if called in future, back to normal
    var masterPackageJSON = JSON.parse(masterFile);
    var workingPackageJSON = JSON.parse(workingFile);

    // has dependency in master that is not in working
    var dependenciesOnlyInMaster = _.difference(
      _.keys(masterPackageJSON.dependencies),
      _.keys(workingPackageJSON.dependencies)
    );
    // has dependency in working that is not in master
    var dependenciesOnlyInWorking = _.difference(
      _.keys(workingPackageJSON.dependencies),
      _.keys(masterPackageJSON.dependencies)
    );
    // has some dependencies that changed version
    var dependenciesInBoth = _.intersection(
      _.keys(workingPackageJSON.dependencies),
      _.keys(masterPackageJSON.dependencies)
    );
    var dependenciesWithDifferentVersions = _.filter(dependenciesInBoth, function (dependencyName) {
      // is local dependency
      if (workingPackageJSON.dependencies[dependencyName].match(/^file\:/)) return false
      // is not local dependency, check version
      else return masterPackageJSON.dependencies[dependencyName] !== workingPackageJSON.dependencies[dependencyName];
    });
    // log alert message if necessary
    if (dependenciesOnlyInMaster.length || dependenciesOnlyInWorking.length || dependenciesWithDifferentVersions.length) {
      modulo.log($$.log.warning, "[PACKAGE.JSON] Modified dependencies in package.json!");
      if (dependenciesOnlyInMaster.length) modulo.log($$.log.error, "The following dependencies are only in master: "+ dependenciesOnlyInMaster.join(", "));
      if (dependenciesOnlyInWorking.length) modulo.log($$.log.error, "The following dependencies are only in working: "+ dependenciesOnlyInWorking.join(", "));
      if (dependenciesWithDifferentVersions.length) {
        modulo.log($$.log.error, "Some dependencies have different versions in working and master: ");
        _.each(dependenciesWithDifferentVersions, function (dependencyName) {
          modulo.log($$.log.error, dependencyName +":\n\tmaster: "+ masterPackageJSON.dependencies[dependencyName] +"\n\tworking: "+ workingPackageJSON.dependencies[dependencyName]);
        });
      };
    };

    // remove dependencies to compare file without them
    delete masterPackageJSON.dependencies;
    delete workingPackageJSON.dependencies;
    if (!_.isEqual(masterPackageJSON, workingPackageJSON)) {
      modulo.log($$.log.warning, "[PACKAGE.JSON] Modified parts of package.json file (master, working):");
      let comparison = $$.compare(masterPackageJSON, workingPackageJSON);
      modulo.log($$.log, comparison.inObj1, comparison.inObj2);
      // git.diff(["master", "working", "package.json"], function (err, result) {
      //   if (err) return done(err);
      //   $$.log.style("Modified package.json file:", "#F0F");
      //   $$.log(result);
      //   if (done) done();
      // });

    };

  }

  //
  //                              ERROR WHILE DOING COMPARISON

  catch (err) {
    modulo.log($$.log.error, "Failed to compare package json file in working and master for module: "+ modulo.name);
    modulo.log($$.log.error, err);
  };

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = async function (modulo, config) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  if (modulo.noBranches) return modulo.log($$.log.error, "This module doesn't have branches, so they can't be compared.");

  try {
    let diffSummary = await modulo.git.diffSummary("master..working");

    let listOfFilesWithPackageJSON = _.pluck(diffSummary.files, "file");
    let [listOfFiles, packageJsonList] = _.partition(listOfFilesWithPackageJSON, function (fileName) { return fileName != "package.json" });

    // log list of modified files
    if (listOfFiles.length) {
      modulo.log($$.log.warning, "[MERGE] Modified files (between working and master):");
      modulo.log($$.log, $$.json.pretty(listOfFiles));
    };

    // log details
    if (config.detailedLogging) {
      modulo.log($$.log);
      modulo.log($$.log.style, "Full diff summary:", "#F0F", true);
      modulo.log($$.log, diffSummary);
    };

    // compare package.json files as well
    if (packageJsonList.length) await comparePackageJsonFile(modulo);

  }
  catch (err) {
    modulo.log($$.log.error, "Failed to diff module: "+ modulo.name);
    modulo.log($$.log.error, err);
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
