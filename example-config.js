
module.exports = {

  absolutePath: "/home/someuser/my-repos/",

  modules: {
    someModule: {
      path: "someModule",
      noBranches: true,
    },
    "some-app": {
      path: "apps/some-app",
    },
    yetAnotherModulePath: "noBranches",
    and_yet_another: "branches",
  },

};
