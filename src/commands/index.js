const _ = require("underscore");
const $$ = require("squeak/node");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: commands that can be executed on a module
  TYPE: <{
    desc: <string> « description of this command (for help page) »,
    func: <function(
      modulo <{
        name: <string> « name of the module »,
        path: <string> « full path to the repository »,
        noBranches: <boolean>,
        git: <simpleGit·return>,
        run: <function(commandName <string>)> « let's you run any command on this module »,
        log: <function()> « log some output for this module »,
      }>,
      !config <gitty·config>,
    )>,
  }[]>
  RETURN: <void>
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              CHECK ALL

  checkall: {
    desc: "Run status, branch and diffs commands.",
    func: require("./checkall"),
  },

  //
  //                              STATUS, BRANCH, DIFFS (COMPONENTS OF ALL)

  status: {
    desc: "Log uncommitted changes.",
    func: require("./status"),
  },

  branch: {
    desc: "Check if current branch is right one, and it's status.",
    func: require("./branch"),
  },

  diffs: {
    desc: "Compare working and master branches.",
    func: require("./diffs"),
  },

  //
  //                              MERGE
  
  merge: {
    desc: "Merge working branch to master.",
    func: require("./merge"),
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
