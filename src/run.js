const _ = require("underscore");
const $$ = require("squeak/node");
const simpleGit = require("simple-git");
let commands = require("./commands");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE MODULO
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeModulo (config, moduleName) {

  // get module object
  let moduleObject = config.modules[moduleName];

  // support passing branches option only (short syntax => relativePath/moduleName: <"master"|"working">)
  if (_.isString(moduleObject)) {
    if (moduleObject !== "working") moduleObject = { noBranches: true }
    else moduleObject = {};
  };

  // missing module path, make it from moduleName
  if (!moduleObject.path) moduleObject.path = moduleName;

  // make modulo object
  let modulo = {
    name: moduleName,
    path: config.absolutePath + moduleObject.path,
    noBranches: moduleObject.noBranches,
    git: simpleGit(config.absolutePath + moduleObject.path),
    run: function (otherCommandName) { return run(config, otherCommandName, modulo); },
    hasLogged: false,
    log: function (loggerToUse) {

      // display introduction log for this module
      if (!modulo.hasLogged) {
        modulo.hasLogged = true;
        $$.log.line("white");
        $$.log.style(modulo.name, "white", true);
        $$.log();
      };

      // display logs
      loggerToUse.apply(this, _.rest(arguments));

    },
  };

  // return modulo object
  return modulo;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: run a given command (supports passing a command name that doesn't exist, or missing module name)
  ARGUMENTS: (
    !config <gitty·config>,
    ?commandName <string>,
    ?moduleNameOrModulo <string|modulo>,
  )
  RETURN:
*/
let run = async function (config, commandName, moduleNameOrModulo) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // unknown command
  if (!commands[commandName]) {
    $$.log.error("Unknown command: "+ commandName);
    $$.log.warning("Valid command names are: '"+ _.keys(commands).join(", ") +"'");
  }
  // moduleNameOrModulo is modulo
  else if (_.isObject(moduleNameOrModulo)) await commands[commandName].func(moduleNameOrModulo, config)
  // unknown module
  else if (!moduleNameOrModulo) $$.log.error("You need to specify the module you want to execute '"+ commandName +"' command on")
  else if (!config.modules[moduleNameOrModulo]) $$.log.error("'"+ moduleNameOrModulo +"' is not a valid module name")
  // run command
  else await commands[commandName].func(makeModulo(config, moduleNameOrModulo), config);

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = run;
