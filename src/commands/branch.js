const _ = require("underscore");
const $$ = require("squeak/node");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = async function (modulo, config) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  try {

    // get branches info
    let branchesReport = await modulo.git.branch();

    // log current branch
    if (!modulo.noBranches && branchesReport.current !== "working") {
      modulo.log($$.log.warning, "Current branch: "+ branchesReport.current);
    };

    // log branches that have unpushed commits
    var aheadBranches = _.filter(branchesReport.branches, function (branche) { return branche.label.match(/^\[ahead/); })
    if (aheadBranches.length) {
      _.each(aheadBranches, function (aheadBranche) {
        modulo.log($$.log.warning, "[PUSH] "+ aheadBranche.name +" is ahead of it's remote —— "+ aheadBranche.label);
      });
    };

    if (config.detailedLogging) {
      modulo.log($$.log);
      modulo.log($$.log.style, "Full branches report:", "#F0F", true);
      modulo.log($$.log, branchesReport);
    };

  }
  catch (err) {
    modulo.log($$.log.error, "Failed to compare branches for module: "+ modulo.name);
    modulo.log($$.log.error, err);
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
